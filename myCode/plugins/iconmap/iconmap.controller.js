(function() {
    'use strict';

    angular
        .module('myapp.iconmap')
        .controller('iconmapController', iconmapController);

    iconmapController.$inject = [
        '$scope',
        'c8yInventory',
        'c8yCepModule' //,
        //'$q',
        //'c8yBinary'
    ];

    function iconmapController(
        $scope,
        //$q,
        c8yInventory,
        //c8yBinary
        c8yCepModule
    ) {
        //todo:
        //$scope.modules = [{name:'张三',url:'www.baidu.com',sop:'张三的sop'},{name:'李四',url:'www.google.com',sop:'李四的sop'},{name:'王五',url:'www.qq.com',sop:'王五的sop'}];
        var id = '';
        showTaiwan();
        $scope.modules = [];
        $scope.managedObject = {
            name: '',
            url: '',
            sop: ''
        };
        $scope.newObj = {};

        $scope.saveInventory = function saveInventory() {
            console.log("xx");
            c8yInventory.save({
                id: id,
                name: 'taiwan',
                info: $scope.newObj //save C
            }).then(function() {
                alert("保存成功")
            });
        };

        function getModules() {
            //getA
            c8yCepModule.list({}).then(function(modules) {
                $scope.modules = modules; //getA
                //getB
                c8yInventory.detail(id).then(function(res) {
                    console.log(res);
                    $scope.managedObject = res.data.info;
                    //makeA&B
                    angular.forEach($scope.modules, function(value, index) {
                        console.log(value.name);
                        $scope.newObj[value.name] = {
                            "name": value.name
                        };
                        angular.forEach($scope.managedObject, function(v, i) {
                            if (value.name == v.name) {
                                $scope.newObj[value.name] = {
                                    "name": v.name,
                                    "url": v.url,
                                    "sop": v.sop
                                }
                            }
                        })
                    })
                });

            });
        }

        function showTaiwan() {
            c8yInventory.list({
                type: "taiwan"
            }).then(function(data) {
                console.log(data);
                if (data[0]) {
                    id = data[0].id;
                    getModules()
                } else {
                    window.addedfns.fn1()
                }
            })
        }
        window.addedfns = {
            fn1: function() {
                c8yInventory.save({
                    type: 'taiwan',
                    name: 'taiwan'
                }).then(function(data) {
                    console.log(data);
                    id = data.id;
                    getModules()
                });
            },
            showTaiwan: function() {
                c8yInventory.list({
                    type: "taiwan"
                }).then(function(data) {
                    if (data.managedObjects[0]) {
                        id = data.managedObjects[0].id;
                        getModules()
                    } else {
                        window.addedfns.fn1()
                    }
                })
            }
        };
        /*
        $scope.markers = [];

        var getDevicesAndBinaries = {
          devices: getDevicesWithLocation(),
          binaries: c8yBinary.list({})
        };
        $q.all(getDevicesAndBinaries).then(placeTypes);

        function getDevicesWithLocation() {
          var filters = {fragmentType: 'c8y_Position' };
          return c8yInventory.list(filters);
        }

        function placeTypes(devicesAndBinaries) {
          var devicesOfType = createTypeMap(devicesAndBinaries.devices);
          var iconOfType = createIconMap(devicesAndBinaries.binaries);
          angular.forEach(devicesOfType, _.curry(placeType)(iconOfType));
        }

        function placeType(iconOfType, devices, type) {
          var icon = iconOfType[type];
          if (icon) {
            var placeDevices = _.curry(place)(devices);
            c8yBinary.downloadAsDataUri(icon).then(placeDevices);
          } else {
            place(devices);
          }
        }

        function createTypeMap(devices) {
          var typeMap = {};
          angular.forEach(devices, _.curry(addDeviceToTypeMap)(typeMap));
          return typeMap;
        }

        function addDeviceToTypeMap(typeMap, device) {
          var hw = 'default';
          if (device.c8y_Hardware && device.c8y_Hardware.model) {
            hw = device.c8y_Hardware.model;
          }

          if (!typeMap[hw]) {
            typeMap[hw] = [];
          }

          typeMap[hw].push(device);
        }

        function createIconMap(binaries) {
          var iconMap = {};
          angular.forEach(binaries, _.curry(addIconToIconMap)(iconMap));
          return iconMap;
        }

        function addIconToIconMap(iconMap, icon) {
          if (c8yBinary.isImage(icon)) {
            var name = icon.name;
            name = name.substring(0, name.lastIndexOf('.'));
            iconMap[name] = icon;
          }
        }

        function place(devices, uri) {
          angular.forEach(devices, _.curry(placeDevice)(_, uri));
        }

        function placeDevice(device, uri) {
          var pos = device.c8y_Position;
          var marker = {
            lat: pos.lat,
            lng: pos.lng,
            message: '<a href="#/device/' + device.id + '">' + device.name + '</a>'
          };

          if (uri) {
            marker.icon = { iconUrl: uri };
          }

          $scope.markers.push(marker);
        }*/
    }
}());
