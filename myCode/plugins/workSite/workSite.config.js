(function() {
    'use strict';

    angular
        .module('myapp.workSite')
        .config(configure);

    configure.$inject = [
        'c8yComponentsProvider',
        'gettext'
    ];

    function configure(
        c8yComponentsProvider,
        gettext
    ) {
        c8yComponentsProvider.add({ // adds a menu item to the widget menu list with ...
            name: 'Work Site', // ... the name *"Work Site"*
            nameDisplay: gettext('中華工程IoT自動回報系統'), // ... the displayed name *"Work Site"*
            description: gettext('展示工地运作情况'), // ... a description
            templateUrl: ':::PLUGIN_PATH:::/views/workSite.main.html', // ... displaying *"workSite.main.html"* when added to the dashboard
            options: {
                noDeviceTarget: true
            }
        });
    }
}());
