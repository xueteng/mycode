create schema LocationAlarmAndGroup(
  alarm Alarm,
  device ManagedObject
);
@Name('Location_alarm_filter')
insert into LocationAlarm
  select
  e.alarm as alarm,
  findFirstManagedObjectParent(e,e.alarm.id) as device
  from AlarmCreated e;
@Name('Count_alarm_at_Group')
insert into UpdateManagedObject
  select
  e.device.id as id
