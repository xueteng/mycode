(function() {
    'use strict';

    angular
        .module('myapp.workSite')
        .controller('workSiteController', workSiteController)
        .controller('ModalInstanceCtrl', modalInstanceCtrl);

    workSiteController.$inject = [
        '$scope',
        'c8yAudits',
        'c8yAlarms',
        'c8yGroups',
        '$q',
        '$http',
        '$uibModal',
        '$log',
        'c8yInventory'
        //     'c8yBinary'
    ];

    function workSiteController(
        $scope,
        c8yAudits,
        c8yAlarms,
        c8yGroups,
        $q,
        $http,
        $uibModal,
        $log,
        c8yInventory
        // c8yBinary
    ) {
        $scope.programeList = [];
        $scope.test = function(a) {
            alert(a)
        };
        var groupList = {
            groups: getGroups()
        };
        $q.all(groupList).then(countAlarms);

        function getGroups() {
            console.log("getGroups");
            return c8yGroups.getTopLevelGroups()
        };

        function getGroupInfo(value, index) {

        };


        function countAlarms(groupArray) {
            console.log(groupArray);
            angular.forEach(groupArray.groups, function(value, index) {
                var item = {};
                item.name = value.name;
                item.id = value.id;
                item.url = ['#/group/', value.id, '/info'].join('');
                c8yAlarms.list({
                    source: value.id,
                    withSourceAssets: true,
                    // withSourceDevices: true,
                    revert: true,
                    withParents: true,
                    resolved: false,
                    withTotalPage: true,
                    pageSize: 100
                }).then(function(data) {
                    console.log(data);
                    // item.activeErrorCount = data.length;
                    item.activeList = [];
                    item.closedList = [];
                    angular.forEach(data, function(value, index) {
                        if (value.status == "ACTIVE") {
                            item.activeList.push(value)
                        } else {
                            item.closedList.push(value)
                        }
                    })
                    $scope.programeList.push(item);
                });
                // c8yAlarms.list({
                //     source: value.id,
                //     withParents: true,
                //     withSourceAssets: true,
                //     revert: true,
                //     // withSourceDevices: true,
                //     withTotalPage: true,
                //     resolved: true,
                //     pageSize: 100
                // }).then(function(data) {
                //     item.closedErrorCount = data.length;
                //     item.closedList = data;
                // });
            })
        }
        // modal
        var $ctrl = this;
        $ctrl.alarms = {};


        $ctrl.open = function(size, list, type) {
            console.log(list);
            $ctrl.alarms.list = [];
            $ctrl.alarms.type = ["未處理警報", "處理中警报"][type];
            angular.forEach(list, function(value, index) {
                var alarm = {
                    type: value.type,
                    id: value.id,
                    count: value.count,
                    text: value.text,
                    // creationTime: new Date(value.creationTime).valueOf(),
                    // firstOccurrenceTime: new Date(value.firstOccurrenceTime).valueOf() + 8 * 3600 * 1000,
                    creationTime: value.creationTime,
                    firstOccurrenceTime: value.firstOccurrenceTime,
                    status: value.status,
                    severity: value.severity,
                    source: value.source
                };
                $ctrl.alarms.list.push(alarm);
            })
            var parentElem = undefined;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $ctrl.alarms;
                    }
                }
            });
            //关闭模态框回调函数
            modalInstance.result.then(function(selectedItem) {
                $ctrl.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // modal end

        // $scope.markers = [];
        //
        // var getDevicesAndBinaries = {
        //     devices: getDevicesWithLocation(),
        //     binaries: c8yBinary.list({})
        // };
        // $q.all(getDevicesAndBinaries).then(placeTypes);

        // function getDevicesWithLocation() {
        //     var filters = {
        //         fragmentType: 'c8y_Position'
        //     };
        //     return c8yInventory.list(filters);
        // }
        //
        // function placeTypes(devicesAndBinaries) {
        //     var devicesOfType = createTypeMap(devicesAndBinaries.devices);
        //     var iconOfType = createworkSite(devicesAndBinaries.binaries);
        //     angular.forEach(devicesOfType, _.curry(placeType)(iconOfType));
        // }
        //
        // function placeType(iconOfType, devices, type) {
        //     var icon = iconOfType[type];
        //     if (icon) {
        //         var placeDevices = _.curry(place)(devices);
        //         c8yBinary.downloadAsDataUri(icon).then(placeDevices);
        //     } else {
        //         place(devices);
        //     }
        // }
        //
        // function createTypeMap(devices) {
        //     var typeMap = {};
        //     angular.forEach(devices, _.curry(addDeviceToTypeMap)(typeMap));
        //     return typeMap;
        // }
        //
        // function addDeviceToTypeMap(typeMap, device) {
        //     var hw = 'default';
        //     if (device.c8y_Hardware && device.c8y_Hardware.model) {
        //         hw = device.c8y_Hardware.model;
        //     }
        //
        //     if (!typeMap[hw]) {
        //         typeMap[hw] = [];
        //     }
        //
        //     typeMap[hw].push(device);
        // }
        //
        // function createworkSite(binaries) {
        //     var workSite = {};
        //     angular.forEach(binaries, _.curry(addIconToworkSite)(workSite));
        //     return workSite;
        // }
        //
        // function addIconToworkSite(workSite, icon) {
        //     if (c8yBinary.isImage(icon)) {
        //         var name = icon.name;
        //         name = name.substring(0, name.lastIndexOf('.'));
        //         workSite[name] = icon;
        //     }
        // }
        //
        // function place(devices, uri) {
        //     angular.forEach(devices, _.curry(placeDevice)(_, uri));
        // }
        //
        // function placeDevice(device, uri) {
        //     var pos = device.c8y_Position;
        //     var marker = {
        //         lat: pos.lat,
        //         lng: pos.lng,
        //         message: '<a href="#/device/' + device.id + '">' + device.name + '</a>'
        //     };
        //
        //     if (uri) {
        //         marker.icon = {
        //             iconUrl: uri
        //         };
        //     }
        //
        //     $scope.markers.push(marker);
        // }
    };
    modalInstanceCtrl.$inject = ['$uibModalInstance', 'c8yAlarms', 'c8yBase', 'c8yAudits', 'items', 'c8yInventory'];

    function modalInstanceCtrl($uibModalInstance, c8yAlarms, c8yBase, c8yAudits, items, c8yInventory) {
        var $ctrl = this;
        var id = '';
        $ctrl.SOPs = [];

        $ctrl.alarms = items;
        $ctrl.currentId = 0;
        $ctrl.currentInfo = {};
        $ctrl.showHistory = [false, false];
        // $ctrl.selected = {
        //     item: $ctrl.alarms[0]
        // };
        showTaiwan();
        //获取规则对应的备注
        function getSOPs() {
            c8yInventory.detail(id).then(function(res) {
                $ctrl.SOPs = res.data.info;
            });
        }

        function showTaiwan() {
            c8yInventory.list({
                type: "taiwan"
            }).then(function(data) {
                if (data[0]) {
                    id = data[0].id;
                    getSOPs()
                } else {
                    window.addedfns.fn1()
                }
            })
        }
        $ctrl.ok = function() {
            $uibModalInstance.close($ctrl.selected.item);
        };

        $ctrl.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $ctrl.showMore = function(p, key) {
            console.log(p.id + key);
            if (p.id + key == $ctrl.currentId) {
                $ctrl.showHistory[key] = !$ctrl.showHistory[key];
            } else {
                $ctrl.showHistory = [false, false];
                $ctrl.showHistory[key] = true;
                $ctrl.currentId = p.id + key;
                console.log(p);
                $ctrl.currentInfo = p;
                console.log(key);
                if (key == 1) {
                    c8yAudits.list({
                        source: p.id,
                        pageSize: 100
                    }).then(function(data) {
                        (data[+data.length - 1].activity == "Availability monitoring record") && (data[+data.length - 1].activity = "Alarm updated", data[+data.length - 1].type = "Alarm");
                        console.log(data);
                        $ctrl.currentInfo.history = data;
                        // if (data[data.length - 1].changes) {
                        //     data[data.length] = data[data.length - 1].changes[0]
                        // }
                    })
                }
            }
        };
        $ctrl.save = function(type) {
            // console.log($ctrl.currentInfo);
            if (type == 0) {
                var params = {
                    severity: $ctrl.currentInfo.severity,
                    // source: {
                    //     id: $ctrl.currentInfo.id
                    // },
                    // type: $ctrl.currentInfo.type,
                    status: "ACKNOWLEDGED",
                    text: $ctrl.content,
                    id: $ctrl.currentInfo.id,
                    time: moment().format(c8yBase.dateFormat)
                };
                console.log(params);
                c8yAlarms.save(params).then(function(data) {
                    console.log(data);
                    // c8yAudits.list({
                    //     source: $ctrl.currentInfo.id,
                    //     pageSize: 100
                    // }).then(function(data) {
                    //     console.log(data);
                    //     $ctrl.currentInfo.history = data;
                    // })
                })
            } else {
                var params = {
                    severity: $ctrl.currentInfo.severity,
                    source: $ctrl.currentInfo.source,
                    // type: $ctrl.currentInfo.type,
                    status: "CLEARED",
                    text: $ctrl.content,
                    id: $ctrl.currentInfo.id,
                    time: moment().format(c8yBase.dateFormat)
                };
                console.log(params);
                c8yAlarms.save(params).then(function(data) {
                    console.log(data);
                })
            }
        }
    };

    // Please note that the close and dismiss bindings are from $uibModalInstance.

    // angular.module('myapp.workSite').component('modalComponent', {
    //     templateUrl: 'myModalContent.html',
    //     bindings: {
    //         resolve: '<',
    //         close: '&',
    //         dismiss: '&'
    //     },
    //     controller: function() {
    //         var $ctrl = this;
    //
    //         $ctrl.$onInit = function() {
    //             $ctrl.alarms = $ctrl.resolve.items;
    //             // $ctrl.selected = {
    //             //     item: $ctrl.alarms[0]
    //             // };
    //         };
    //
    //         $ctrl.ok = function() {
    //             $ctrl.close({
    //                 $value: $ctrl.selected.item
    //             });
    //         };
    //
    //         $ctrl.cancel = function() {
    //             $ctrl.dismiss({
    //                 $value: 'cancel'
    //             });
    //         };
    //     }
    // });
}());
